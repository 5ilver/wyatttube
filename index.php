<html>
<head>
<link rel="shortcut icon" href="images/favicon.png" >
<title>WyattTube - Videography and conversation with a W on it</title>
<body>

<?php
$mode="";
$file = "info/videos";
$videolist = file($file);
if ( isset($_GET['id'])  && in_array($_GET['id']."\n",$videolist) ){
    $id=$_GET['id'];
}else{
    $id="default";
}
if (isset($_GET['newcomment'])){
    $file = "comments/" . $id;
    $fp = fopen($file,'a');
    $message = $_POST['usermessage'];
    $message = filter_var($message, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
    $message = date('l \t\h\e jS \of F Y h:i:s A')."<br>".substr($message,0,300)."\n";
    fwrite($fp, $message); 
    fclose($fp);
    $mode="comments";
} elseif (isset($_GET['delcomment'])){
    $file = "comments/" . $id;
    $fp = fopen($file,'a');
    $d = $_GET['commentid'];
    $i = 0;
    $data = file($file);
    $out = array();
    foreach($data as $line) {
        $out[] = $line;
    }
    $fp = fopen($file, "w+");
    flock($fp, LOCK_EX);
    foreach($out as $line) {
        if ($i != $d){
            fwrite($fp, $line);
        }
        $i = $i + 1;
    }
    flock($fp, LOCK_UN);
    
    fclose($fp);
    $mode="comments";
} elseif (isset($_GET['comments'])){
	$mode="comments";
} elseif (isset($_GET['uploader'])){
    $mode="uploader";
} elseif (isset($_GET['upload'])){
    $target_dir = "videos/";
    $id = uniqid();
    $target_file =  $id . ".webm";
    $uploadOk = 1;
    // Check if file already exists
    if (file_exists($target_file)) {
        echo "Sorry, file already exists.";
        $uploadOk = 0;
    }
    // Check file size
    if ($_FILES["file"]["size"] > 500000000) {
        echo "Sorry, your file is too large. 500000000 bytes plox.";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
    // if everything is ok, try to upload file
    } else {
        
        shell_exec("/usr/bin/ffmpeg -i " . $_FILES["file"]["tmp_name"] . " -vcodec libvpx -cpu-used -5 -deadline realtime " . $target_dir . $target_file);
        $fp = fopen("info/videos",'a');
        fwrite($fp, $id."\n"); 
        fclose($fp);
        $fp = fopen("info/".$id,'w');
        $videoname = filter_var($_POST['videoname'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        $videoname = substr($videoname,0,30);
        $videoname = "$videoname\n";
        fwrite($fp, $videoname);
        fwrite($fp, "0\n");
        fwrite($fp, "$target_file\n");
        fwrite($fp, date('l \t\h\e jS \of F Y h:i:s A')."\n"); 
        fwrite($fp, "0\n");
        fwrite($fp, "0\n");
        fwrite($fp, "0\n");
        fclose($fp);
        $mode="video";
        $file = "info/videos";
        $videolist = file($file);
    
    }
} elseif (isset($_GET['delvideo'])){
    $file = "info/videos";
    $fp = fopen($file, "w");
    foreach($videolist as $line) {
        if ($id . "\n" != $line){
            fwrite($fp, $line);
        }
    }
    fclose($fp);
    $videolist = file($file);
    $mode="home";
} elseif (isset($_GET['like'])){
	$mode="comments";
	$file = "info/" . $id;
    $data = file($file);
    $data[4] = (int)$data[4] + 1;
    $data[4] = $data[4] . "\n";
    $fp = fopen($file,'w');
    foreach($data as $line) {
        fwrite($fp, $line); 
    }
fclose($fp); 
} elseif (isset($_GET['dislike'])){
	$mode="comments";
	$file = "info/" . $id;
$data = file($file);
$data[5] = (int)$data[5] + 1;
$data[5] = $data[5] . "\n";
$fp = fopen($file,'w');
foreach($data as $line) {
fwrite($fp, $line); 
}
fclose($fp); 
}elseif (isset($_GET['video'])){
    	$mode="video";
}else{
    $mode="home";   
}

if ($mode=="comments"){
$file = "info/" . $id;
$data = file($file);
?>


<div id="stats">
<i>Views: <?php echo $data[1]; ?> </i>
<br>
<a href="?like=1&id=<?php echo $id; ?>">+Likes</a>: <?php echo $data[4]; ?> 
<br>
<a href="?dislike=1&id=<?php echo $id; ?>"> - Dislikes</a>: <?php echo $data[5]; ?>
<br>
<?php
$file = "comments/" . $id;
$data = file($file);
?>
<br>
</div>

<div id="comments">
<?php
$i=0;
foreach($data as $line) {
$line = stripslashes($line);
?>


<div class="comment">
<table>
<tr>
<td>
<img src="images/favicon.png" width="100" height="100">
</td>
<td>
<?php
echo $line;
?>
<br>
<a href="?delcomment=1&commentid=<?php echo $i; ?>&id=<?php echo $id; ?>">[delete]</a>
</td>
</tr>
</table>
</div>


<?php
echo "\n";
$i++;
}
?>
</div>
<center>


<div id="newcomment">
<form action="?newcomment=1&id=<?php echo $id; ?>" method="post">
<input type="text" name="usermessage">
<input type="submit" value="Post Comment">
</form>
</div>

</center>

<?php
}else{

?>



<div id="head">
<div style="margin-left: 20px; float: right;">
<a href="?uploader=true&id=<?php echo $id; ?>">Upload a new video</a><br><br>
</div>
<div id="logo">
<a href="?"><img src="images/logo.png"></a>
</div>
</div>
<?php

    if ($mode=="video"){
    $file = "info/" . $id;
    $data = file($file);
    $data[1] = (int)$data[1] + 1;
    $data[1] = $data[1] . "\n";
    $fp = fopen($file,'w');
    foreach($data as $line) {
        fwrite($fp, $line); 
    }
    fclose($fp); 

?>


<div id="content" style="width: 600px; float: left;">
<div id="title">
<h1><?php echo $data[0]; ?></h1>
<p>Uploaded: <?php echo $data[3]; ?></>
<i><a href="?delvideo=1&id=<?php echo $id; ?>">[remove]</a></i>
</div>
<div id="video">
<video controls=true src="videos/<?php echo str_replace(array("\r", "\n"), '', $data[2]); ?>" width="600" height="400"></video>
</div>
<iframe frameborder="0" src="?comments=1&id=<?php echo $id; ?>" width="600" height="800">comments</iframe>
</div>

<div id="sidebar" style="margin-left: 620px;">



<div id="suggestions">
<?php
foreach(array_reverse($videolist) as $vidline) {
$vidline = stripslashes($vidline);
$thumbfile = "info/" . str_replace(array("\r", "\n"), '', $vidline);
$thumbdata = file($thumbfile);
?>
<div class="suggestion">
<a href="?video=1&id=<?php echo str_replace(array("\r", "\n"), '', $vidline); ?>">
<table>
<tr>
<td>
<div  style="width: 100px; float: left;">
<video src="videos/<?php echo str_replace(array("\r", "\n"), '', $thumbdata[2]); ?>" width="100" height="100"></video>
</div>
</td>
<td>
<div  style="width: 300px;">
<?php
echo $thumbdata[0];
?>
<br>
Likes: <?php echo $thumbdata[4]; ?> - Dislikes: <?php echo $thumbdata[5]; ?>
</div>
</td>
</tr>
</table>
</a>
<?php
}
?>
</div>

<?php
    }

    if ($mode=="home"){
?>

<div id="homesuggestions">
<?php
foreach(array_reverse($videolist) as $vidline) {
$vidline = stripslashes($vidline);
$thumbfile = "info/" . str_replace(array("\r", "\n"), '', $vidline);
$thumbdata = file($thumbfile);
?>
<div class="suggestion" style="margin-bottom:40px;vertical-align:text-top;width:400px;display:inline-block;text-align:center;">
<a href="?video=1&id=<?php echo str_replace(array("\r", "\n"), '', $vidline); ?>">
<center>
<video src="videos/<?php echo str_replace(array("\r", "\n"), '', $thumbdata[2]); ?>" width="100" height="100"></video>
<br>
<?php
echo $thumbdata[0];
?>
<br>
Likes: <?php echo $thumbdata[4]; ?> - Dislikes: <?php echo $thumbdata[5]; ?>
</center>
</a>
</div>
<?php
}
?>
</div>



<?php
    }

    if ($mode=="uploader"){
?>
<br>
<br>
<center>
<form action="?upload=true" method="post" enctype="multipart/form-data">
    Select file to upload (300000000b limit):
    <br>
    <input type="hidden" name="MAX_FILE_SIZE" value="300000000" />
    <br>
    <input type="file" name="file">
    <br>
    <br>
    <br>
    <br>
    What do we call this fine masterpiece (30 character limit)?
    <br>
    <br>
    <input type="text" name="videoname">
    <br>
    <br>
    <br>    
    <br>
    <input type="submit" value="Upload!" name="submit">
</form>
<br>
<br>
Don't want to upload after all? <a href="?video=1&id=<?php echo $id; ?>">Go back.</a>
</center>
<?php
    }

}
?>
</body>
</html>
