![Wyatt Tube Logo](images/logo.png)

a simple clone of a popular video sharing site.

Features:

* Upload a video
* Delete a video
* Post comment on video
* Delete comment on video
* Like video
* Dislike video
* Video view count

Here are the basic steps to configure Wyatt Tube 

First install apache, ffmpeg, and php.

sudo apt-get install apache2 ffmpeg php -y

Now you should have a folder at /var/www/html with an index.html file in it. Remove that file and move this stuff over there.

sudo rm /var/www/html/index.html

sudo mv images /var/www/html/

sudo mv index.php /var/www/html/

Now cd /var/www/html and set the permissions for this to be able to read and write files in comments, videos, and info folders.

sudo mkdir comments/

sudo mkdir videos/

sudo mkdir info/

sudo chown www-data -R comments info videos

sudo chmod 775 comments -R images info videos

You'll probably want to set up an htaccess file with rules to require authentication if this is public, otherwise you should be good to go for a local network. Just hit http://localhost/ if you installed on the same machine as your browser is running on. Php's default limits for http uploads are pretty small so you may want to raise them in your php.ini to allow larger videos to be uploaded.
